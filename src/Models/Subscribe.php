<?php
/*
 * @Author: SanQian
 * @Date: 2021-09-06 17:18:02
 * @LastEditTime: 2021-09-07 11:13:13
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_kuaidi100/src/Models/Subscribe.php
 *
 */

namespace Zhaohangyang\ToolsPhpKuaidi100\Models;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpKuaidi100\Kuaidi100Basic;

class Subscribe extends Kuaidi100Basic
{

    public $headers = [
        'content-type' => 'application/x-www-form-urlencoded',
    ];

    public function subscribeRequest($subscribe_body)
    {
        $pai_url = '/poll'; //实时查询请求地址

        $body = [
            // schema    String    是    json    返回的数据格式
            // Θparam    param    是        由其他字段拼接
            // └ company    String    是    ems    订阅的快递公司的编码，一律用小写字母
            // └ number    String    是    1136281381675    订阅的快递单号，单号的最大长度是32个字符 下载编码表格
            // └ from    String    否    广东省深圳市南山区    出发地城市，省-市-区，非必填，填了有助于提升签收状态的判断的准确率，请尽量提供
            // └ to    String    否    北京市朝阳区    目的地城市，省-市-区，非必填，填了有助于提升签收状态的判断的准确率，且到达目的地后会加大监控频率，请尽量提供
            // └ key    String    是        授权码，请申请企业版获取
            // Θparameters    parameters    是        附加参数信息
            // └- callbackurl    String    是        回调接口的地址
            // └- salt    String    否    XXXXXXXXXX    签名用随机字符串
            // └- resultv2    String    否    1    添加此字段表示打开行政区域解析功能
            // └- autoCom    String    否    1    添加此字段且将此值设为1，则表示开启智能判断单号所属公司的功能，开启后，需确保company字段为空,即只传运单号（number字段），我方收到后会根据单号判断出其所属的快递公司（即company字段）。建议只有在无法知道单号对应的快递公司（即company的值）的情况下才开启此功能。
            // └- interCom    String    否    1    添加此字段表示开启国际版，开启后，若订阅的单号（即number字段）属于国际单号，会返回出发国与目的国两个国家的跟踪信息，本功能暂时只支持邮政体系（国际类的邮政小包、EMS）内的快递公司，若单号我方识别为非国际单，即使添加本字段，也不会返回destResult元素组
            // └- departureCountry    String    否    CN    出发国家编码
            // └- departureCom    String    否    ems    出发的快递公司的编码
            // └- destinationCountry    String    否    JP    目的国家编码
            // └- destinationCom    String    否    japanposten    目的的快递公司的编码
            // └- phone    String    否    13488888888    收、寄件人的电话号码（手机和固定电话均可，只能填写一个，顺丰速运和丰网速运必填，其他快递公司选填。如座机号码有分机号，分机号无需上传。）
        ] + $subscribe_body;

        $request = new Request('POST', $pai_url, $this->headers, $this->getPostData($body));
        return $this->requestJsonSync($request);
    }
}
