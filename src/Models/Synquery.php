<?php
/*
 * @Author: SanQian
 * @Date: 2021-09-06 17:18:02
 * @LastEditTime: 2021-09-07 11:24:43
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_kuaidi100/src/Models/Synquery.php
 *
 */

namespace Zhaohangyang\ToolsPhpKuaidi100\Models;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpKuaidi100\Kuaidi100Basic;

class Synquery extends Kuaidi100Basic
{

    public $headers = [
        'content-type' => 'application/x-www-form-urlencoded',
    ];

    public function get($get_body)
    {
        $api_url = '/poll/query.do'; //实时查询请求地址

        $body = [
            // └ com    string    是    yuantong    查询的快递公司的编码， 一律用小写字母 下载编码表格
            // └ num    string    是    12345678    查询的快递单号， 单号的最大长度是32个字符
            // └ phone    string    否    13888888888    收、寄件人的电话号码（手机和固定电话均可，只能填写一个，顺丰速运和丰网速运必填，其他快递公司选填。如座机号码有分机号，分机号无需上传。）
            // └ from    string    否    广东深圳    出发地城市
            // └ to    string    否    北京朝阳    目的地城市，到达目的地后会加大监控频率
            // └ resultv2    int    否    1    添加此字段表示开通行政区域解析功能。0：关闭（默认），1：开通行政区域解析功能以及物流轨迹增加物流状态值，2：开通行政解析功能以及物流轨迹增加物流状态值并且返回出发、目的及当前城市信息 4：开通行政解析功能以及物流轨迹增加物流高级状态名称并且返回出发、目的及当前城市信息 6:开通行政解析功能以及物流轨迹增加物流高级状态名称、状态值并且返回出发、目的及当前城市信息
            // └show    String    否    0    返回格式：0：json格式（默认），1：xml，2：html，3：text
            // └order    String    否    desc    返回结果排序:desc降序（默认）,asc 升序
        ] + $get_body;

        $request = new Request('POST', $api_url, $this->headers, $this->getPostData($body));
        return $this->requestJsonSync($request);
    }
}
